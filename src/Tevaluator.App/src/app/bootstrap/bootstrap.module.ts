import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BsButtonComponent } from './bs-button/bs-button.component';
import { BsDropdownComponent } from './bs-dropdown/bs-dropdown.component';
import { BsInputComponent } from './bs-input/bs-input.component';
import { BsNavbarComponent } from './bs-navbar/bs-navbar.component';
import { RouterModule } from '@angular/router';
import { FooterComponent } from './footer/footer.component';



@NgModule({
  declarations: [BsButtonComponent, BsDropdownComponent, BsInputComponent, BsNavbarComponent, FooterComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [BsButtonComponent, BsDropdownComponent, BsInputComponent, BsNavbarComponent, FooterComponent],
})
export class BootstrapModule { }
