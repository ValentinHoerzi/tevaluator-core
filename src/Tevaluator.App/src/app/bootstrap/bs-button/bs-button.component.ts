import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'bs-button',
  templateUrl: './bs-button.component.html',
  styleUrls: ['./bs-button.component.scss']
})
export class BsButtonComponent implements OnInit {

  @Input() mode = 'primary';
  @Input() size = 'lg';
  @Input() isDisabled = false;
  @Output() clicked = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

  onClick(ev: any): void {
    this.clicked.emit(ev);
  }
}
