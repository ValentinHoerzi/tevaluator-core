import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'bs-dropdown',
  templateUrl: './bs-dropdown.component.html',
  styleUrls: ['./bs-dropdown.component.scss']
})
export class BsDropdownComponent implements OnInit {

  colors: string[] = ['green', 'orange', 'yellow', 'red', 'black'];
  @Input() selectedColor = '#6c757d';
  @Output() colorSelected: EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  onColorSelected(color: string): void {
    this.selectedColor = color.toLocaleLowerCase();
    this.colorSelected.emit(color);
  }
}
