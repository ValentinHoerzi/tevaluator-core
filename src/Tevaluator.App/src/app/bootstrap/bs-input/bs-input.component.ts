import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'bs-input',
  templateUrl: './bs-input.component.html',
  styleUrls: ['./bs-input.component.scss']
})
export class BsInputComponent implements OnInit {

  @Input() note = '';
  @Output() changed = new EventEmitter<string>();
  showInput = false;

  constructor() { }

  ngOnInit(): void {
  }

  valueChanged(val: string): void {
    this.changed.emit(val);
    this.showInput = !this.showInput;
  }
}
