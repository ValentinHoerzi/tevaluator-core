import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CoreService {


  API = 'http://localhost:8080/students';


  constructor(private service: HttpClient) { }


}
