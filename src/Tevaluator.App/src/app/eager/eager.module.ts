import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EagerRoutingModule } from './eager-routing.module';
import { BootstrapModule } from '../bootstrap/bootstrap.module';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ErrorComponent } from './error/error.component';

@NgModule({
  declarations: [HomeComponent, LoginComponent, SignUpComponent, ErrorComponent],
  imports: [
    CommonModule,
    BootstrapModule,
    FormsModule,
    ReactiveFormsModule,
    EagerRoutingModule,
  ],
})
export class EagerModule {}
