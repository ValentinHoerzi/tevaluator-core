export enum Roles{
    Admin = 'Admin',
    X = 'X',
    XX = 'XX',
    XXX = 'XXX',
    XXXX = 'XXXX',
    FM = 'FM',
    PhilX = 'PhilX',
    PhilXX1 = 'PhilXX_1',
    PhilXX2 = 'PhilXX_2',
    PhilXXX = 'PhilXXX',
    PhilXXXX = 'PhilXXXX',
    Philister = 'Philister',
    Bursch = 'Bursch'
}