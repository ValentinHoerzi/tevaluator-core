export class User{
    id: number;
    firstname: string;
    lastname: string;
    username: string;
    role: string;
    age: number;
    colourName: string;
    token: string; 
}