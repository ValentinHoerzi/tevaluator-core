﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TevaluatorDb
{
    public class Answer
    {
        public int Id { get; set; }
        public string _Answer { get; set; }
        public string Note { get; set; }
        public int Score { get; set; }
        public User Student { get; set; }
    }
}
