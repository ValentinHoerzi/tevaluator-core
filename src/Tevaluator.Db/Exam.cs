﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TevaluatorDb
{
    public class Exam
    {
        public int Id { get; set; }
        public string Desc { get; set; }
        public string Date { get; set; }
        public List<Question> Questions { get; set; }
        public List<Answer> Answers { get; set; }
    }
}
