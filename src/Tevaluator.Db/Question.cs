﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TevaluatorDb
{
    public class Question
    {
        public int Id { get; set; }
        public string _Question { get; set; }
        public User Teacher { get; set; }
    }
}
