﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace TevaluatorDb
{
    public class TevaluatorContext : DbContext
    {
        public TevaluatorContext()
        {

        }

        public TevaluatorContext(DbContextOptions<TevaluatorContext> options): base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            var connectionString = "data source=(LocalDB)\\mssqllocaldb; attachdbfilename=D:\\Daten\\MyProjects\\Tevaluator-Core\\src\\Tevaluator.Db\\TevaluatorDb.mdf; database=Tevaluator; integrated security=True; MultipleActiveResultSets=True";
            options.UseSqlServer(connectionString);
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Exam> Exams { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Answer> Answers { get; set; }


    }
}
