﻿using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using Tevaluator.Models.dtos;

namespace Tevaluator.Assets
{
    public static class ExtantionMethods
    {
        public static bool IsAdmin(this AuthenticationResource authenticationDto) => authenticationDto.Role == "Admin";

        public static AuthenticationResource GetUserAuthInfo(this ControllerBase controller)
        {
            var claimIdentity = controller.HttpContext.User.Identity as ClaimsIdentity;
            string role = claimIdentity?.FindFirst(ClaimTypes.Role)?.Value ?? "Unknown";
            return new AuthenticationResource
            {
                Id = int.Parse(claimIdentity?.FindFirst(ClaimTypes.NameIdentifier)?.Value),
                Firstname = claimIdentity?.FindFirst(ClaimTypes.GivenName)?.Value,
                Lastname = claimIdentity?.FindFirst(ClaimTypes.Surname)?.Value,
                Username = claimIdentity?.FindFirst(ClaimTypes.Name)?.Value,
                Role = role,
            };
        }
    }
}
