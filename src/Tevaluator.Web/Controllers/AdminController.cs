﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Reflection;
using Tevaluator.Assets;
using Tevaluator.Models.dtos;
using Tevaluator.Services;

namespace Tevaluator.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly AdminService service;

        public AdminController(AdminService service)
        {
            this.service = service;
        }

        [HttpGet]
        public ActionResult<AuthenticationResource> GetAdminData()
        {
            Console.WriteLine($"AdminController::{MethodBase.GetCurrentMethod().Name}");
            var userAuthInfo = this.GetUserAuthInfo();
            if (!userAuthInfo.IsAdmin()) return Forbid();

            return userAuthInfo;
        }


        [HttpDelete("e98176df2be819e08a72febad7db986dc887f08761d2764cbae5ffc05ece0a1f")]
        public ActionResult<string> ResetDatabase()
        {
            Console.WriteLine($"AdminController::{MethodBase.GetCurrentMethod().Name}");
            var userAuthInfo = this.GetUserAuthInfo();
            if (!userAuthInfo.IsAdmin()) return Forbid();

            service.ResetDatabase();

            return "Database-Reset Successful!";
        }
    }
}
