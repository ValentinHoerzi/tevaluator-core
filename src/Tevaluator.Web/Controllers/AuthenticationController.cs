﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Tevaluator.Models.dtos;
using Tevaluator.Services;
using TevaluatorDb;

namespace Tevaluator.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly UserService userService;
        private readonly AppSettings appSettings;

        public AuthenticationController(UserService userService, IOptions<AppSettings> appSettings)
        {
            this.userService = userService;
            this.appSettings = appSettings.Value;
        }


        [AllowAnonymous]
        [HttpPost]
        public ActionResult<AuthenticationResource> Authenticate([FromBody] UserLoginDto userDto)
        {
            Console.WriteLine($"AuthenticationController::{MethodBase.GetCurrentMethod().Name}");

            var user = userService.Authenticate(userDto.Username, userDto.Password);
            if (user == null) return Unauthorized();

            string tokenString = CreateTokenString(user);

            return Ok(new AuthenticationResource
            {
                Id = user.Id,
                Username = user.Username,
                Age = user.Age,
                ColourName = user.ColourName,
                Firstname = user.Firstname,
                Lastname = user.Lastname,
                Role = user.Role,
                Token = tokenString,
            });
        }

        private string CreateTokenString(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                    new Claim(ClaimTypes.Name,user.Username),
                    new Claim(ClaimTypes.GivenName, user.Firstname),
                    new Claim(ClaimTypes.Surname,user.Lastname),
                    new Claim(ClaimTypes.Role,user.Role),
                }),
                Expires = DateTime.UtcNow.AddHours(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);
            return tokenString;
        }
    }
}