﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Reflection;
using Tevaluator.Models.dtos;
using Tevaluator.Models.resources;
using Tevaluator.Services;

namespace Tevaluator.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    [ApiController]
    public class ExamController : ControllerBase
    {
        private readonly ExamService examService;

        public ExamController(ExamService examService)
        {
            this.examService = examService;
        }

        [HttpGet]
        public int GetExamsCount()
        {
            Console.WriteLine($"ExamController::{MethodBase.GetCurrentMethod().Name}");

            return examService.GetAllExams().Count;
        }


        [HttpGet]
        public List<ExamResource> GetAllExams()
        {
            Console.WriteLine($"ExamController::{MethodBase.GetCurrentMethod().Name}");

            return examService.GetAllExams();
        }

        [HttpPost]
        public ExamResource AddExam([FromBody] ExamDto dto)
        {
            Console.WriteLine($"ExamController::{MethodBase.GetCurrentMethod().Name}");

            return examService.AddExam(dto);
        }

        [HttpPost]
        public QuestionResource AddQuestion([FromBody] QuestionDto dto)
        {
            Console.WriteLine($"ExamController::{MethodBase.GetCurrentMethod().Name}");

            return examService.AddQuestion(dto);
        }

        [HttpPost]
        public AnswerResource AddAnswer([FromBody] AnswerDto dto)
        {
            Console.WriteLine($"ExamController::{MethodBase.GetCurrentMethod().Name}");

            return examService.AddAnswer(dto);
        }
    }
}
