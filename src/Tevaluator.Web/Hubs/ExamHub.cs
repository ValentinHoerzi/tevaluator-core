﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tevaluator.Hubs
{
    public class ExamHub : Hub
    {
        public ExamHub()
        {
        }

        public void NewExam(object exam)
        {

        }

        public override Task OnConnectedAsync()
        {
            Console.WriteLine($"Client Connected: {Context.ConnectionId}");
            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            Console.WriteLine($"Client Disconnected: {Context.ConnectionId}");
            return base.OnDisconnectedAsync(exception);

        }
    }
}
