﻿namespace Tevaluator.Models.dtos
{
    public class AnswerDto
    {
        public string _Answer { get; set; }
        public string Note { get; set; }
        public int StudentId { get; set; }
        public int ExamId { get; set; }
    }
}