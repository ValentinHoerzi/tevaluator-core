﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tevaluator.Models.dtos
{
    public class ExamDto
    {
        public string Desc { get; set; }
        public string Date { get; set; }
    }
}
