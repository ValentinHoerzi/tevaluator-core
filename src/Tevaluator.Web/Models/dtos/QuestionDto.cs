﻿namespace Tevaluator.Models.dtos
{
    public class QuestionDto
    {
        public string _Question { get; set; }
        public int TeacherId { get; set; }
        public int ExamId { get; set; }

    }
}