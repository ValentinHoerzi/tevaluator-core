﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tevaluator.Models.dtos;

namespace Tevaluator.Models.resources
{
    public class AnswerResource
    {
        public int Id { get; set; }
        public string _Answer { get; set; }
        public string Note { get; set; }
        public int Score { get; set; }
        public UserResource Student { get; set; }
    }
}
