﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tevaluator.Models.resources
{
    public class ExamResource
    {
        public int Id { get; set; }
        public string Desc { get; set; }
        public string Date { get; set; }
        public List<QuestionResource> Questions { get; set; }
        public List<AnswerResource> Answers { get; set; }
    }
}
