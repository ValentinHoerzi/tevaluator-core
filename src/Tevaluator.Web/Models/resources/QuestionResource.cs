﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tevaluator.Models.dtos;

namespace Tevaluator.Models.resources
{
    public class QuestionResource
    {
        public int Id { get; set; }
        public string _Question { get; set; }
        public UserResource Teacher { get; set; }
    }
}
