﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tevaluator.Models.dtos
{
    public class UserResource
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Username { get; set; }
        public string Role { get; set; }
        public int Age { get; set; }
        public string ColourName { get; set; }
    }
}
