﻿using TevaluatorDb;
using Microsoft.EntityFrameworkCore;


namespace Tevaluator.Services
{
    public class AdminService
    {
        private readonly TevaluatorContext db;

        public AdminService(TevaluatorContext db)
        {
            this.db = db;
        }

        internal void ResetDatabase()
        {
            //db.Database.ExecuteSqlRaw("TRUNCATE TABLE ANSWERS");
            //db.Database.ExecuteSqlRaw("TRUNCATE TABLE QUESTIONS");
            //db.Database.ExecuteSqlRaw("TRUNCATE TABLE EXAMS");
            db.SaveChanges();
        }
    }
}
