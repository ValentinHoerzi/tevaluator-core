﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Tevaluator.Models.dtos;
using Tevaluator.Models.resources;
using TevaluatorDb;

namespace Tevaluator.Services
{
    public class ExamService
    {
        private readonly TevaluatorContext db;

        public ExamService(TevaluatorContext db)
        {
            this.db = db;
        }

        internal List<ExamResource> GetAllExams() => db.Exams
            .Include(x => x.Questions).ThenInclude(x => x.Teacher)
            .Include(x => x.Answers).ThenInclude(x => x.Student)
            .ToList()
            .Select(x => MapExamToExamResource(x))
            .ToList();


        internal ExamResource AddExam(ExamDto dto)
        {
            var exam = new Exam { Desc = dto.Desc, Date = dto.Date };
            var dbExam = db.Exams.Add(exam).Entity;
            db.SaveChanges();
            return MapExamToExamResource(dbExam);
        }

        internal QuestionResource AddQuestion(QuestionDto dto)
        {
            var teacher = db.Users.FirstOrDefault(x => x.Id == dto.TeacherId);
            var question = new Question { _Question = dto._Question, Teacher = teacher };
            var dbQuestion = db.Questions.Add(question).Entity;
            db.SaveChanges();
            var dbExam = db.Exams.FirstOrDefault(x => x.Id == dto.ExamId);

            if (dbExam.Questions == null)
            {
                dbExam.Questions = new List<Question>();
                dbExam.Questions.Add(dbQuestion);
            }
            else
            {
                dbExam.Questions.Add(dbQuestion);
            }

            db.SaveChanges();
            return MapQuestionToQuestionResource(dbQuestion);
        }

        internal AnswerResource AddAnswer(AnswerDto dto)
        {
            var student = db.Users.FirstOrDefault(x => x.Id == dto.StudentId);

            var answer = new Answer
            {
                _Answer = dto._Answer,
                Note = dto.Note,
                Student = student,
                Score = 0,
            };

            var dbAnswer = db.Answers.Add(answer).Entity;
            db.SaveChanges();
            var dbExam = db.Exams.FirstOrDefault(x => x.Id == dto.ExamId);

            if (dbExam.Answers == null)
            {
                dbExam.Answers = new List<Answer>();
                dbExam.Answers.Add(dbAnswer);
            }
            else
            {
                dbExam.Answers.Add(dbAnswer);
            }

            db.SaveChanges();
            return MapAnswerToAnswerResource(dbAnswer);
        }

        private ExamResource MapExamToExamResource(Exam exam)
        {
            List<QuestionResource> questionResources = new List<QuestionResource>();
            questionResources = exam.Questions?.Select(x => MapQuestionToQuestionResource(x)).ToList();

            List<AnswerResource> answerResources = new List<AnswerResource>();
            answerResources = exam.Answers?.Select(x => MapAnswerToAnswerResource(x)).ToList();

            return new ExamResource
            {
                Id = exam.Id,
                Desc = exam.Desc,
                Date = exam.Date,
                Answers = answerResources,
                Questions = questionResources,
            };
        }

        private AnswerResource MapAnswerToAnswerResource(Answer answer) => new AnswerResource
        {
            Id = answer.Id,
            Note = answer.Note,
            Score = answer.Score,
            _Answer = answer._Answer,
            Student = MapUserToUserResource(answer.Student),
        };

        private QuestionResource MapQuestionToQuestionResource(Question question) => new QuestionResource
        {
            Id = question.Id,
            _Question = question._Question,
            Teacher = MapUserToUserResource(question.Teacher),
        };

        private UserResource MapUserToUserResource(User student) => new UserResource
        {
            Id = student.Id,
            Firstname = student.Firstname,
            Lastname = student.Lastname,
            Username = student.Username,
            Age = student.Age,
            ColourName = student.ColourName,
            Role = student.Role,
        };
    }
}
