﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TevaluatorDb;

namespace Tevaluator.Services
{
    public class UserService
    {

        private readonly List<User> tempUsers = new List<User>
        {
            new User{Username = "vHTango", Role="Admin", Firstname ="Valentin", Lastname ="Hörzi", Age = 19, ColourName = "Tango", Password ="1234"},
            new User{Username = "aKBaldini", Role="Bursch", Firstname ="Andreas", Lastname ="Kalt", Age = 18, ColourName = "Baldini", Password ="1234"},
        };

        private readonly TevaluatorContext db;

        public UserService(TevaluatorContext db)
        {
            this.db = db;
        }

        public User Authenticate(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password)) return null;

            if (db.Users.Count() == 0)
            {
                foreach (var u in tempUsers)
                {
                    db.Users.Add(u);
                }
                db.SaveChanges();
            }


            var user = db.Users.SingleOrDefault(x => x.Username == username);
            //var user = tempUsers.SingleOrDefault(x => x.Username == username);

            if (user == null) return null;
            return password == user.Password ? user : null;
        }
    }
}
